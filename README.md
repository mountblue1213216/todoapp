This is a demo todolist project, it resembles a todo list format, which anyone can use as it has been prepared, by keeping some of the most important code principles and project management recommendations. Feel free to change anything.

- src -> Inside the src folder all the actual source code regarding the project will reside, this will not include any kind of tests. (You might want to make separate tests folder)

- routes -> In the routes folder, we register a route and the corresponding middleware and controllers to it.

- controllers -> they are kind of the last middlewares as post them you call you business layer to execute the business logic. In controllers we just receive the incoming requests and data and then pass it to the business layer, and once business layer returns an output, we structure the API response in controllers and send the output.

* validations -> this includes the validation of the request body and the url parameters, it is essential for checking the user input, otherwise any error caused due to user input, might result in the crashing of the server.

# Pre-requisites

The project uses the following tech-stack-

- Node.js(Javascript)
- Postgres Database
- ORM prisma

# Setup the project

- Download this template from github and open it in your favourite text editor. Go inside the folder path and execute the following command:

  npm install

  In the root directory create a .env file and add the following env variables

      PORT=<port number of your choice>

  ex:

      PORT=3000

* If you're setting up your development environment, then write the username of your db, password of your db of the postgres database. Refer to .env example file for further information

* To run the server execute

  npm start
